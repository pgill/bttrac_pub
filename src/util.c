/*const
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#include <arpa/inet.h>
#include <stdlib.h>

#include "util.h"

char *btt_util_sockaddr_to_ip(struct sockaddr *sa) {

  size_t address_length;
  void *sin_addr;

  switch (sa->sa_family) {
  case AF_INET:
    address_length = INET_ADDRSTRLEN;
    sin_addr = &(((struct sockaddr_in *)sa)->sin_addr);
    break;
  case AF_INET6:
    address_length = INET6_ADDRSTRLEN;
    sin_addr = &(((struct sockaddr_in6 *)sa)->sin6_addr);
    break;
  default:
    return 0;
  }

  char *ip_address_buffer = malloc(address_length);
  if(!ip_address_buffer){
    return 0;
  }

  if (!inet_ntop(sa->sa_family, sin_addr, ip_address_buffer, address_length)) {
    free(ip_address_buffer);
    return 0;
  }
  return ip_address_buffer;
}
