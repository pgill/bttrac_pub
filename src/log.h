/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_LOG_H
#define BTTRAC_LOG_H

void btt_log_debug(const char *message);

void btt_log_console(const char *message);

#endif
