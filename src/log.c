/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#include <stdio.h>

#include "log.h"

void btt_log_debug(const char *message) {
#ifdef BTT_DEBUG
  btt_log_console(message);
#endif
}

void btt_log_console(const char *message) {
  if (!message) {
    return;
  }
  printf("%s\n", message);
}
