/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#include <errno.h>
#include <signal.h>
#include <stdlib.h>

#include "condition.h"
#include "log.h"
#include "tracker.h"

#define BTT_DEFAULT_PORT 22222

static btt_condition_t *btt_exit_condition = 0;

static void btt_main_signal_handler_post(int single_id) {
  btt_log_debug("btt_main_signal_handler_post: waiting...");
}

static void btt_main_signal_handler(int single_id) {
  signal(single_id, btt_main_signal_handler_post);
  btt_log_debug("btt_main_signal_handler: siginal received...");
  if (btt_exit_condition) {
    btt_condition_signal(btt_exit_condition);
  }
}

int main(int argc, char **argv) {

  btt_exit_condition = btt_condition_create();
  if (!btt_exit_condition) {
    return 1;
  }
  signal(SIGINT, btt_main_signal_handler);
  int exit_code = 0;
  unsigned short port = BTT_DEFAULT_PORT;

  if (argc > 1) {
    long int custom_port = strtol(argv[1], 0, 10);
    if (errno == ERANGE || !(custom_port > 1023 && custom_port < 65535) ) {
      btt_log_console("main: invalid port, using default port");
    } else {
      port = (unsigned short)custom_port;
    }
  }
  btt_tracker_t *tracker = btt_tracker_start(port);

  if (tracker) {
    btt_condition_wait(btt_exit_condition);
    btt_tracker_stop(tracker);
    tracker = 0;
  } else {
    exit_code = 1;
  }

  btt_condition_destroy(btt_exit_condition);
  return exit_code;
}
