/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_CONDITION_H
#define BTTRAC_CONDITION_H

typedef struct btt_condition btt_condition_t;

btt_condition_t *btt_condition_create();

void btt_condition_wait(btt_condition_t *condition);

void btt_condition_signal(btt_condition_t *condition);

void btt_condition_destroy(btt_condition_t *condition);

#endif
