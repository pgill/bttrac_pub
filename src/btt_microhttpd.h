/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_MICROHTTPD_H
#define BTTRAC_MICROHTTPD_H

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/types.h>

#ifndef MHD_PLATFORM_H
#define MHD_PLATFORM_H
#include <microhttpd.h>
#endif

#endif