/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#include <stdio.h>
#include <string.h>

#include "const.h"
#include "handler.h"
#include "log.h"
#include "util.h"

#define GREETING_FORMAT "d14:failure reason%lu:%se"

int btt_handler_handle_with_empty_response(struct MHD_Connection *connection,
                                           unsigned int http_status_code) {
  struct MHD_Response *response =
      MHD_create_response_from_buffer(0, 0, MHD_RESPMEM_MUST_COPY);
  int result = MHD_queue_response(connection, http_status_code, response);
  MHD_destroy_response(response);
  return result;
}

int btt_handler_is_matching_request(const char *expected_method,
                                    const char *actual_method,
                                    const char *expected_url,
                                    const char *acutal_url) {

  size_t expected_method_len = strlen(expected_method);
  size_t actual_method_len = strlen(actual_method);

  if (expected_method_len != actual_method_len ||
      strncmp(expected_method, actual_method, expected_method_len)) {
    return 0;
  }

  size_t expected_url_len = strlen(expected_url);
  size_t actual_url_len = strlen(acutal_url);
  return expected_url_len == actual_url_len &&
         !strncmp(expected_url, acutal_url, expected_url_len);
}

char *btt_handler_get_default_message(char *ip_address) {

  size_t ip_address_len = strlen(ip_address);
  size_t message_len = (strlen(GREETING_FORMAT) - 5) + 2 /* size */ + ip_address_len + 1;
  char *message = malloc(sizeof(char) * (message_len));

  snprintf(message, message_len, GREETING_FORMAT, ip_address_len, ip_address);
  btt_log_debug(message);
  return message;
}

int btt_handler_handle(struct MHD_Connection *connection, const char *url,
                       const char *method) {
  btt_log_debug("btt_handler_handle:");

  if (!btt_handler_is_matching_request("GET", method, "/", url)) {
    btt_log_debug("btt_handler_handle: unknown request");
    return btt_handler_handle_with_empty_response(connection,
                                                  MHD_HTTP_NOT_FOUND);
  }

  const union MHD_ConnectionInfo *connection_info =
      MHD_get_connection_info(connection, MHD_CONNECTION_INFO_CLIENT_ADDRESS);
  char *ip_address = btt_util_sockaddr_to_ip(connection_info->client_addr);
  if (!ip_address) {
    btt_log_debug("btt_handler_handle: unknown client ip address");
    return btt_handler_handle_with_empty_response(connection,
                                                  MHD_HTTP_NOT_FOUND);
  }

  char *response_content = btt_handler_get_default_message(ip_address);
  free(ip_address);

  if (response_content) {
    struct MHD_Response *response = MHD_create_response_from_buffer(
        strlen(response_content), response_content, MHD_RESPMEM_MUST_FREE);

    MHD_add_response_header(response, "Content-Type", BTT_MIME_PLAIN);
    int result = MHD_queue_response(connection, MHD_HTTP_OK, response);
    MHD_destroy_response(response);
    return result;
  }

  btt_log_debug("btt_handler_handle: unable to craft response");
  return btt_handler_handle_with_empty_response(connection,
                                                MHD_HTTP_INTERNAL_SERVER_ERROR);
}
