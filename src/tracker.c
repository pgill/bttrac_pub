/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */

#include "btt_microhttpd.h"
#include "handler.h"
#include "log.h"
#include "tracker.h"

typedef struct btt_tracker {
  struct MHD_Daemon *daemon;
} btt_tracker_t;

static int btt_tracker_default_handler_callback(
    void *cls, struct MHD_Connection *connection, const char *url,
    const char *method, const char *version, const char *upload_data,
    size_t *upload_data_size, void **con_cls) {
  btt_log_debug("btt_tracker_default_handler_callback:");
  return btt_handler_handle(connection, url, method);
}

btt_tracker_t *btt_tracker_start(unsigned short port) {

  struct MHD_OptionItem mhd_options[] = {{MHD_OPTION_END, 0, NULL}};

  struct MHD_Daemon *daemon =
      MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, port, NULL, NULL,
                       btt_tracker_default_handler_callback, NULL,
                       MHD_OPTION_ARRAY, mhd_options, MHD_OPTION_END);

  btt_tracker_t *instance = 0;

  if (daemon) {
    btt_log_debug("btt_tracker_start: started daemon");
    instance = malloc(sizeof(btt_tracker_t));
    if (!instance) {
      btt_log_debug("btt_tracker_start: instance allocation failed");
      MHD_stop_daemon(daemon);
      return 0;
    }
    instance->daemon = daemon;
  } else {
    btt_log_debug("btt_tracker_start: failed to start daemon");
  }
  return instance;
}

void btt_tracker_stop(btt_tracker_t *tracker) {
  if (tracker) {
    if (tracker->daemon) {
      MHD_socket socket = MHD_quiesce_daemon(tracker->daemon);
      MHD_stop_daemon(tracker->daemon);
      shutdown(socket, SHUT_RDWR);
      btt_log_debug("btt_tracker_stop: stopped daemon");
    }
    free(tracker);
    tracker = 0;
  }
}
