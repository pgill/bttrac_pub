/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_HANDLER_H
#define BTTRAC_HANDLER_H

#include "btt_microhttpd.h"

int btt_handler_handle(struct MHD_Connection *connection, const char *url,
                       const char *method);

#endif
