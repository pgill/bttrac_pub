/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#include <pthread.h>
#include <stdlib.h>

#include "condition.h"

typedef struct btt_condition {
  pthread_cond_t *cond;
  pthread_mutex_t *mutex;
} btt_condition_t;

btt_condition_t *btt_condition_create() {

  btt_condition_t *condition = 0;
  pthread_cond_t *cond = malloc(sizeof(pthread_cond_t));
  if (pthread_cond_init(cond, NULL)) {
    free(cond);
    return condition;
  }

  pthread_mutex_t *mutex = malloc(sizeof(pthread_mutex_t));
  if (pthread_mutex_init(mutex, NULL)) {
    free(mutex);
    pthread_cond_destroy(cond);
    free(cond);
    return condition;
  }

  condition = malloc(sizeof(btt_condition_t));
  if (!condition) {
    pthread_mutex_destroy(mutex);
    free(mutex);
    pthread_cond_destroy(cond);
    free(cond);
    return condition;
  }
  condition->cond = cond;
  condition->mutex = mutex;
  return condition;
}

void btt_condition_wait(btt_condition_t *condition) {
  if (condition && condition->cond && condition->mutex) {
    pthread_mutex_lock(condition->mutex);
    pthread_cond_wait(condition->cond, condition->mutex);
    pthread_mutex_unlock(condition->mutex);
  }
}

void btt_condition_signal(btt_condition_t *condition) {
  if (condition && condition->cond && condition->mutex) {
    pthread_mutex_lock(condition->mutex);
    pthread_cond_signal(condition->cond);
    pthread_mutex_unlock(condition->mutex);
  }
}

void btt_condition_destroy(btt_condition_t *condition) {
  if (condition) {
    if (condition->cond) {
      pthread_cond_destroy(condition->cond);
      free(condition->cond);
    }

    if (condition->mutex) {
      pthread_mutex_destroy(condition->mutex);
      free(condition->mutex);
    }
    free(condition);
  }
}
