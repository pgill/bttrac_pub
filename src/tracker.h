/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_TRACKER_H
#define BTTRAC_TRACKER_H

typedef struct btt_tracker btt_tracker_t;

btt_tracker_t *btt_tracker_start(unsigned short port);

void btt_tracker_stop(btt_tracker_t *tracker);

#endif
