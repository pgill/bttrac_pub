/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_UTIL_H
#define BTTRAC_UTIL_H

#include <sys/socket.h>

char *btt_util_sockaddr_to_ip(struct sockaddr *sa);

#endif
