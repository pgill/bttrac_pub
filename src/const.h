/*
 * This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
 * 1991.
 */
#ifndef BTTRAC_CONST_H
#define BTTRAC_CONST_H

#define BTT_MIME_PLAIN "text/plain;charset=utf-8"

#endif