#
# This code is licenced under the GNU GENERAL PUBLIC LICENSE Version 2, June
# 1991.
#
#!/bin/env bash

BUILD_DIR=./out

if [ -d "$BUILD_DIR" ]
then
	rm -rf $BUILD_DIR
fi

BUILD_TYPE="${1:-Debug}"

cmake -H./src -B$BUILD_DIR -DCMAKE_BUILD_TYPE=$BUILD_TYPE
cmake --build $BUILD_DIR --target all
